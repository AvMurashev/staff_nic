from django.contrib import admin

from .models import StaffUnit, Project, Graphik, Working, StaffStatus

class StaffUnitAdmin(admin.ModelAdmin):

    list_display = ('id', 'unit_name', 'unit_position', 'unit_salary' )

class ProjectAdmin(admin.ModelAdmin):

    list_display = ('id', 'name_project', 'address_object', 'start_project', 'end_project', 'start_time', 'end_time')

class GraphikAdmin(admin.ModelAdmin):

    list_display = ('work_date', 'unit_name', 'start_work', 'stop_work')
    list_filter = ('unit_name', 'work_date')

class StaffStatusAdmin(admin.ModelAdmin):

    list_display = ('staff_unit', 'project', 'status')
    list_filter = ('staff_unit', 'project', 'status')

admin.site.register(StaffUnit, StaffUnitAdmin)
admin.site.register(Project, ProjectAdmin)
admin.site.register(Graphik, GraphikAdmin)
admin.site.register(Working)
admin.site.register(StaffStatus, StaffStatusAdmin)

# Register your models here.
