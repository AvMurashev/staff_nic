from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class StaffUnit(models.Model):
    unit_name = models.CharField(max_length=200, verbose_name='ФИО')
    unit_position = models.CharField(max_length=200, verbose_name='должность')
    unit_salary = models.IntegerField(verbose_name='почасовая ставка')
    unit_mounth_salary = models.FloatField(default=0, verbose_name='заработная плата в мес')
    unit_user = models.ForeignKey(User, on_delete=models.CASCADE, default=1, verbose_name='Логин')
    flag = models.BooleanField(default=True)
    copy_password = models.CharField(max_length=21, verbose_name='копия пароля', default='IvaN123!')

    class Meta:
        verbose_name = 'Сотрудники'
        verbose_name_plural = 'Сотрудники'

    def __str__(self):
        return self.unit_name

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            CustomUser.objects.get(username=username)
        except CustomUser.DoesNotExist:
            return username
        raise forms.ValidationError(self.error_messages['duplicate_username'])


class Project(models.Model):
    name_project = models.CharField(max_length=200, verbose_name='название проекта')
    address_object = models.CharField(max_length=200, verbose_name='адрес')
    curator_project = models.ForeignKey(StaffUnit, on_delete=models.CASCADE, related_name='curator', verbose_name='куратор')
    gen_architector = models.ForeignKey(StaffUnit, on_delete=models.CASCADE, related_name='gen_architector', verbose_name='архитектор')
    start_project = models.DateField( verbose_name='начало проекта')
    end_project = models.DateField( verbose_name='конец проекта')
    start_time = models.TimeField(verbose_name='время открытия объекта')
    end_time = models.TimeField(verbose_name='время закрытие объекта')
    staff_list = models.ManyToManyField(StaffUnit, related_name='staff_list', verbose_name='список прикрепленных сотрудников')
    flag = models.BooleanField(default=True)

    class Meta:
        verbose_name = 'Проекты'
        verbose_name_plural = 'Проекты'

    def __str__(self):
        return self.name_project

class StaffStatus(models.Model):
    STATUS = (
        ('active', 'Активный'),
        ('negative', 'Неактивный'),
        ('temp', 'Временно неактивный'),
        ('health', 'Болеет'),
        )
    status = models.CharField(max_length=200, verbose_name='статус', choices=STATUS, default='active')
    staff_unit = models.ForeignKey(StaffUnit, on_delete=models.CASCADE, verbose_name='ФИО')
    project = models.ForeignKey(Project, on_delete=models.CASCADE, verbose_name='название проекта')

    class Meta:
        verbose_name = 'Статусы'
        verbose_name_plural = 'Статусы'

    def __str__(self):
        return self.staff_unit.unit_name



class Graphik(models.Model):
    work_date = models.DateField(verbose_name='дата работы')
    start_work = models.TimeField(verbose_name='время начало работы')
    stop_work = models.TimeField(verbose_name='время окончания работы')
    name_project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='project', verbose_name='название проекта')
    unit_name = models.ForeignKey(StaffUnit, on_delete=models.CASCADE, related_name='unit', verbose_name='Сотрудник')
    unit_hours_day_amount = models.FloatField(verbose_name='кол-во отработанных часов за день')
    unit_hour_salary = models.FloatField(default=0, verbose_name='часовая ставка на дату смены')
    status_pay = models.BooleanField(default=False)

    class Meta:
        verbose_name = 'Графики'
        verbose_name_plural = 'Графики'

    def __str__(self):
        return str(self.work_date)

class Working(models.Model):
    graphik_id = models.IntegerField(verbose_name='ID графика')
    staff_id = models.IntegerField(verbose_name='ID сотрудника')
    project_id = models.IntegerField(verbose_name='ID проекта')

    class Meta:
        verbose_name = 'Активные рабочие сессия'
        verbose_name_plural = 'Активные рабочие сессии'
