from datetime import datetime, timedelta, date
import random


def change_status(data):
    if data == "Временно неактивный":
        result = "temp"
    elif data == "Болеет":
        result = "health"
    elif data == "Активный":
        result = "active"
    elif data == "Неактивный":
        result = "negative"
    else:
        result = None
    return result

def checking_interval_date(date_from, date_to):
    date_from = datetime.strptime(date_from, "%Y-%m-%d").date()
    date_to = datetime.strptime(date_to, "%Y-%m-%d").date()
    if date_from > date_to:
        return None
    else:
        return True

def get_first_mounth_date():
    now = date.today()
    m = now.strftime('%m')
    y = now.strftime('%Y')
    d = '01'
    result = f"{y}-{m}-{d}"
    return result


def get_end_mounth_date():
    now = date.today()
    m = now.strftime('%m')
    y = now.strftime('%Y')
    if m == '01' or m == '03' or m == '05' or m == '07' or m == '08' or m == '10' or m == '12':
        d = '31'
    elif m == '04'or m == '06' or m == '09' or m == '11':
        d = '30'
    elif m == '02':
        if (int(y) % 4) == 0:
            d = '29'
        else:
            d = '28'
    result = f"{y}-{m}-{d}"
    return result

def check_staff_form(unit_name, unit_position, unit_salary, unit_mounth_salary, username, password):
    if len(unit_name) == 0:
        return False
    if len(unit_position) == 0:
        return False
    if unit_salary == '':
        return False
    if unit_mounth_salary == '':
        return False
    if len(username) == 0:
        return False
    if len(password) == 0:
        return False
    return True

def check_project_form(name_project, address_object, curator, \
    architector, start_project, end_project, start_time, end_time):
    """Check all elements of project form beforet saving. Return errors list or empty list"""
    messages_error=[]
    #print('name - ', name_project, type(name_project))
    #print('address - ', address_object, type(address_object))
    #print('curator - ', curator, type(curator))
    #print('architector - ', architector, type(architector))
    #print('start_p - ', start_project, type(start_project))
    #print('end project -', end_project, type(end_project))
    #print('start_time - ', start_time, type(start_time))
    #print('end time - ', end_time, type(end_time))
    if name_project == '':
        messages_error.append('Поле "Название проекта" не должно быть пустым')
    if address_object == '':
        messages_error.append('Поле "Адрес проекта" не должно быть пустым')
    if curator == '':
        messages_error.append('Необходимо выбрать куратора проекта')
    if architector == '':
        messages_error.append('Необходимо выбрать архитектора проекта')
    if start_project == '':
        messages_error.append('Необходимо установить дату начала проекта')
    if end_project == '':
        messages_error.append('Необходимо установить дату окончания проекта')
    if start_time == '':
        messages_error.append('Необходимо установить время открытия объекта')
    if end_time == '':
        messages_error.append('Необходимо установить время закрытия объекта')
    return messages_error

def genpassword():
    chars_low = 'abcdefghijklnopqrstuvwxyz'
    chars_up = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    numbers = '1234567890'
    symbols = '!@#$&*'
    result = ''
    i = 0
    while i <= 7:
        if i < 2:
            result += random.choice(numbers)
        elif i < 4:
            result += random.choice(chars_up)
        elif i < 7:
            result += random.choice(chars_low)
        elif i == 7:
            result += random.choice(symbols)
        i += 1
    print(result)
    return result
