from django.apps import AppConfig


class ManagingConfig(AppConfig):
    name = 'managing'
    verbose_name = 'Управление'
