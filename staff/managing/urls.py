from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:staff_id>/staff/', views.staff, name='staff'),
    path('<int:project_id>/project/', views.project, name='project'),
    path('general/', views.general, name='general'),
    path('login/', views.logining, name='logining'),
    path('<int:staff_id>/staff_detail/', views.staff_detail, name='staff_detail'),
    path('<int:staff_id>/staff_detail/report/', views.report, name='report'),
    path('projects_list/', views.projects_list, name='projects_list'),
    path('staff_list/', views.staff_list, name='staff_list'),
    path('<int:staff_id>/staff_detail/change/', views.staff_change, name='staff_change'),
    path('staff_detail/add/', views.staff_add, name='staff_add'),
    path('<int:project_id>/project/project_change/', views.project_change, name='project_change'),
    path('all_graphiks/', views.all_graphiks, name='all_graphiks'),
    path('sessions/', views.active_session, name='active_session'),
    path('errors_page/', views.errors_page, name='errors_page'),
    path('project_add/', views.project_add, name='project_add'),
    path('reports/', views.reports, name="reports"),
    path('<int:staff_id>/graphchange/', views.graphchange, name='graphchange'),
]
