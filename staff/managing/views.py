from django.http import HttpResponse, HttpResponseRedirect
from managing.models import StaffUnit, Project, Graphik, Working, StaffStatus
from django.db.models import Sum
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login
from datetime import datetime, timedelta
from django.contrib.auth.models import User
from managing.funcmodel import *
import re


@csrf_protect
def index(request):
    return HttpResponseRedirect(reverse('logining'))


@csrf_protect
def staff(request, staff_id):
    if request.user.is_authenticated:
        staff = StaffUnit.objects.get(id=staff_id)
        if staff.flag == False:
            return HttpResponseRedirect(reverse('logining'))

    if request.user.is_authenticated:
        staff = StaffUnit.objects.get(id=staff_id)
        if request.user.id == staff.unit_user.id or request.user.is_superuser:
            response = f"It's {staff.unit_name}"
            now = datetime.utcnow() + timedelta(hours=3)
            wdate = now.date()
            try:
                graphiks_day = Graphik.objects.filter(unit_name=staff, work_date__range=[wdate, wdate])
                graphiks_day = graphiks_day.aggregate(total=Sum('unit_hours_day_amount'))
            except:
                print('Erorr')
            try:
                active_project = StaffStatus.objects.filter(staff_unit=staff, status='active')[0].project
                pstart_time = active_project.start_time.strftime('%H:%M')
                pend_time = active_project.end_time.strftime('%H:%M')
                graphiks_all = Graphik.objects.filter(unit_name=staff, name_project=active_project)
                graphiks_all = graphiks_all.aggregate(total=Sum('unit_hours_day_amount'))
            except:
                active_project = None
            try:
                active_session = Working.objects.filter(staff_id=staff.id)
                start_active = Graphik.objects.get(id=active_session[0].graphik_id).start_work
                start_active = start_active.strftime('%H:%M')
            except:
                active_session = None
            try:
                working = Working.objects.get(staff_id=staff_id)
                start_work = Graphik.objects.get(id=working.graphik_id).start_work
                amount = round(float((now - datetime.combine(wdate, start_work)).seconds) / 3600 * 8/9, 1)
            except:
                print("No start_work")

            if request.POST:
                project_name = active_project.name_project

                if "start" in request.POST:
                    wtime = now.time().strftime("%H:%M:%S")
                    start_work = wtime
                    try:
                        cur_work = Working.objects.get(staff_id=staff_id)
                    except:
                        cur_work = False
                    if cur_work:
                        error_answer = "Вы не можете приступить к новой работе пока не завершите предыдущую"
                    else:
                        g = Graphik(work_date=wdate, start_work=wtime, stop_work=wtime, \
                        name_project=active_project, unit_name=staff,
                        unit_hour_salary=staff.unit_salary, unit_hours_day_amount=0)
                        g.save()
                        w = Working(graphik_id=g.id, staff_id=staff.id, project_id=active_project.id)
                        w.save()
                    return HttpResponseRedirect(reverse('staff', kwargs={'staff_id':staff.id}))
                elif "end" in request.POST: # TODO get id current work
                    try:
                        working = Working.objects.get(staff_id=staff_id)
                        start_time = Graphik.objects.get(id=working.graphik_id).start_work
                        wtime = start_time.strftime("%H:%M:%S")
                        start_time = datetime.combine(wdate, start_time)
                        amount = round(float((now - start_time).seconds) / 3600 * 8/9, 1)
                        end_time = now.strftime("%H:%M:%S")
                        etime = now.strftime("%H:%M")
                        end_time = datetime.strptime(end_time, "%H:%M:%S")
                        print(amount)
                        Graphik.objects.filter(id=working.graphik_id).update(stop_work=end_time, unit_hours_day_amount=amount)
                        working.delete()
                    except:
                        error = "У вас нет незавершенной работы"
                        print('The current work does not exist for this unit')
                    print("PUTS END BUTTON")
                    return HttpResponseRedirect(reverse('staff', kwargs={'staff_id':staff.id}))
            return render(request, 'managing/staff.html', locals())
    return HttpResponseRedirect(reverse('logining'))


@csrf_protect
def project(request, project_id):
    if request.user.is_authenticated and request.user.is_superuser:
        project = Project.objects.get(id=project_id)
        response = f"This project is {project.name_project}"
        staff_statuses = []
        for unit in project.staff_list.all():
            status = StaffStatus.objects.filter(project=project, staff_unit=unit)[0].get_status_display()
            person = (unit, status)
            print(status)
            staff_statuses.append(person)
        return render(request, 'managing/project.html', locals())
    else:
        return render(request, 'managing/login.html')


@csrf_protect
def general(request):
    if request.user.is_authenticated and request.user.is_superuser:
        staff = StaffUnit.objects.all()
        projects = Project.objects.all()
        units = []
        for project in projects:
            units.append(project.staff_list.all())
        return render(request, 'managing/general.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def all_graphiks(request):
    if request.user.is_authenticated and request.user.is_superuser:
        graphiks = Graphik.objects.all()
        shifts = []
        for shift in graphiks:
            print(shift.work_date)
            shifts.append({
            'work_date':format(shift.work_date, '%Y-%m-%d'),
            'name_project':shift.name_project,
            'unit_name': shift.unit_name,
            'start_work': shift.start_work.strftime('%H:%M'),
            'stop_work': shift.stop_work.strftime('%H:%M'),
            'unit_hours_day_amount':shift.unit_hours_day_amount,
            'unit_hour_salary': shift.unit_hour_salary,
            'id':shift.id,
            }
            )
        return render(request, 'managing/all_graphiks.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def active_session(request):
    if request.user.is_authenticated and request.user.is_superuser:
        sessions = Working.objects.all()
        graphiks = []
        for session in sessions:
            graphiks.append(Graphik.objects.get(id=session.graphik_id))
        return render(request, 'managing/sessions.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))



@csrf_protect
def staff_detail(request, staff_id):
    if request.user.is_authenticated and request.user.is_superuser:
        staff = StaffUnit.objects.get(id=staff_id)
        graphik = Graphik.objects.all().filter(unit_name=staff)
        return render(request, 'managing/staff_detail.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def graphchange(request, staff_id):
    if request.user.is_authenticated and request.user.is_superuser:
        staff = StaffUnit.objects.get(id=staff_id)
        graphik = Graphik.objects.all().filter(unit_name=staff)
        working_shifts = []
        i = 0
        for shift in graphik:
            i += 1
            working_shifts.append({
            'work_date':format(shift.work_date, '%Y-%m-%d'),
            'name_project':shift.name_project,
            'start_work':shift.start_work.strftime('%H:%M'),
            'stop_work':shift.stop_work.strftime('%H:%M'),
            'unit_hours_day_amount':shift.unit_hours_day_amount,
            'id':shift.id,
            'number': str(i)
            }
            )
        if request.POST:
            id = re.split('save_', str(request.POST))[1].split("': ")[0]
            start_work = request.POST[f'start_{id}']
            stop_work = request.POST[f'stop_{id}']
            unit_hours_day_amount = round(float((datetime.strptime(stop_work, '%H:%M') - datetime.strptime(start_work, '%H:%M')).seconds / 3600) * 8/9, 1)
            Graphik.objects.filter(id=id).update(start_work=start_work, stop_work=stop_work, unit_hours_day_amount=unit_hours_day_amount)
            return HttpResponseRedirect(reverse('graphchange', kwargs={'staff_id':staff.id}))
        return render(request, 'managing/graphchange.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def staff_add(request):
    if request.user.is_authenticated and request.user.is_superuser:
        if 'generate' in request.POST:
            genpass = genpassword()
            unit_name = request.POST['unit_name']
            unit_position = request.POST['unit_position']
            unit_salary = request.POST['unit_salary']
            unit_mounth_salary = request.POST['unit_mounth_salary']
            username = request.POST['unit_user']

        if 'save' in request.POST:
            unit_name = request.POST['unit_name']
            unit_position = request.POST['unit_position']
            try:
                unit_salary = float(request.POST['unit_salary'].replace(",", "."))
            except:
                unit_salary = ''
                message_salary = 'Необходимо ввести ставку в числовом формате'
            try:
                unit_mounth_salary = float(request.POST['unit_mounth_salary'].replace(",", "."))
            except:
                unit_mounth_salary = ''
                message_msalary = 'Необходимо ввести месячную зарплату в числовом формате'
            username = request.POST['unit_user']
            password = request.POST['unit_password']
            if User.objects.filter(username=username).exists():
                message_username = 'Логин с таким имененм уже суеществует, придумайте другой.'
            if check_staff_form(unit_name, unit_position, unit_salary, unit_mounth_salary, username, password) and not User.objects.filter(username=username).exists():
                unit_user = User.objects.create_user(username=username, password=password)
                unit_user.save()
                staff = StaffUnit(unit_name=unit_name, unit_position=unit_position, unit_salary=unit_salary, unit_mounth_salary=unit_mounth_salary, unit_user=unit_user, copy_password=password)
                staff.save()
                return HttpResponseRedirect(reverse('staff_list'))
            elif not check_staff_form(unit_name, unit_position, unit_salary, unit_mounth_salary, username, password):
                message = "Все поля должны быть заполнены"

        return render(request, 'managing/staff_add.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def staff_change(request, staff_id):
    if request.user.is_authenticated and request.user.is_superuser:
        projects = Project.objects.all()
        staff = StaffUnit.objects.get(id=staff_id)
        graphik = Graphik.objects.all().filter(unit_name=staff)
        user_id = staff.unit_user.id
        unit_user=User.objects.get(id=user_id)
        try:
            status = StaffStatus.objects.filter(staff_unit=staff).exclude(status='negative')
            staff.project = status[0].project.name_project
            staff.status_project = status[0].get_status_display()
        except:
            staff.project_change = 'Нет активных проеков'

        if 'save' in request.POST:
            unit_id = staff.id
            unit_name = request.POST['unit_name']
            unit_position = request.POST['unit_position']
            unit_salary = request.POST['unit_salary']
            unit_mounth_salary = float(request.POST['unit_mounth_salary'].replace(",", "."))
            unit_password = request.POST['unit_password']
            print(unit_password)
            unit_user.set_password(unit_password)
            unit_user.save()
            if 'status' in request.POST:
                status_change = change_status(request.POST['status'])
                StaffStatus.objects.filter(id=status[0].id).update(status=status_change)

            elif 'projects' in request.POST:
                project_new = request.POST['projects']
                if not 'Нет активных проеков' in project_new:
                    project_new = Project.objects.get(name_project=project_new)
                    if staff not in project_new.staff_list.filter():
                        project_new.staff_list.add(staff)
                    if len(StaffStatus.objects.filter(project=project_new, staff_unit=staff)) > 0:
                        StaffStatus.objects.filter(project=project_new, staff_unit=staff).update(status="active")
                    else:
                        status_new = StaffStatus(staff_unit=staff, project=project_new, status="active")
                        status_new.save()

            StaffUnit.objects.filter(id=unit_id).update(unit_name=unit_name, \
                unit_position=unit_position, unit_salary=unit_salary, \
                unit_mounth_salary=unit_mounth_salary, copy_password=unit_password)
            staff = StaffUnit.objects.get(id=staff_id)
            try:
                status = StaffStatus.objects.filter(staff_unit=staff).exclude(status='negative')
                staff.project = status[0].project.name_project
                staff.status_project = status[0].get_status_display()
            except:
                staff.project = 'Нет активных проеков'
            return HttpResponseRedirect(reverse('staff_list'))
        elif 'del' in request.POST:
            status_change = StaffStatus.objects.filter(staff_unit=staff).exclude(status='negative')
            for status in status_change:
                status.status = 'negative'
                status.save()
            staff.flag = False
            staff.save()
            try:
                work = Working.objects.get(staff_id=staff.id)
                try:
                    now = datetime.utcnow() + timedelta(hours=3)
                    start_time = Graphik.objects.get(id=work.graphik_id).start_work
                    wtime = start_time.strftime("%H:%M:%S")
                    wdate = now.date()
                    start_time = datetime.combine(wdate, start_time)
                    now = datetime.utcnow() + timedelta(hours=3)
                    amount = round(float((now - start_time).seconds) / 3600 * 8/9, 1)
                    end_time = now.strftime("%H:%M:%S")
                    end_time = datetime.strptime(end_time, "%H:%M:%S")
                    Graphik.objects.filter(id=work.graphik_id).update(stop_work=end_time, unit_hours_day_amount=amount)
                    work.delete()
                except:
                    print('The current work does not exist for this unit')
            except:
                print("No Working")
            return HttpResponseRedirect(reverse('staff_list'))
        return render(request, 'managing/staff_change.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))


@csrf_protect
def logining(request):
    if request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = None
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                if user.is_superuser:
                    print("Superuser")
                    login(request, user)
                    return HttpResponseRedirect(f'/managing/general/')
                else:
                    print("User is valid, active and authenticated")
                    unit = StaffUnit.objects.get(unit_user=user)
                    print(unit.id)
                    login(request, user)
                    return HttpResponseRedirect(f'/managing/{unit.id}/staff/')

    return render(request, 'managing/login.html', locals())

@csrf_protect
def project_change(request, project_id):
    if request.user.is_authenticated and request.user.is_superuser:
        all_unit_list = StaffUnit.objects.all()
        project = Project.objects.get(id=project_id)
        first_date = project.start_project.strftime('%Y-%m-%d')
        end_date = project.end_project.strftime('%Y-%m-%d')
        first_time = project.start_time.strftime('%H:%M')
        end_time = project.end_time.strftime('%H:%M')
        units = project.staff_list.all().exclude(flag=False)
        try:
            for unit in units:
                unit.status = StaffStatus.objects.filter(staff_unit=unit, project=project)[0].get_status_display()
        except:
            print(units)
        list_of_available = []
        for unit in StaffUnit.objects.all().exclude(flag=False):
            if not (StaffStatus.objects.filter(staff_unit=unit, status='active') \
                or StaffStatus.objects.filter(staff_unit=unit, status='temp') \
                or StaffStatus.objects.filter(staff_unit=unit, status='health')):
                list_of_available.append(unit)

        if request.POST:
            if 'del' in request.POST:
                for unit in units:
                    status = StaffStatus.objects.filter(staff_unit=unit, project=project)[0]
                    status.status = 'negative'
                    status.save()
                if Working.objects.filter(project_id=project.id):
                    for work in Working.objects.filter(project_id=project.id):
                        try:
                            now = datetime.utcnow() + timedelta(hours=3)
                            start_time = Graphik.objects.get(id=work.graphik_id).start_work
                            wtime = start_time.strftime("%H:%M:%S")
                            wdate = now.date()
                            start_time = datetime.combine(wdate, start_time)
                            now = datetime.utcnow() + timedelta(hours=3)
                            amount = round(float((now - start_time).seconds) / 3600, 1) * 8/9
                            end_time = now.strftime("%H:%M:%S")
                            end_time = datetime.strptime(end_time, "%H:%M:%S")
                            Graphik.objects.filter(id=work.graphik_id).update(stop_work=end_time, unit_hours_day_amount=amount)
                            work.delete()
                        except:
                            print('The current work does not exist for this unit')
                project.flag = False
                project.save()
                return HttpResponseRedirect(reverse('projects_list'))
            elif 'save' in request.POST:
                for i, j in enumerate(request.POST.getlist('status')): # checking statuses before and after
                    if j != units[i].status: # if statuse was changed
                        if units[i].status == "Неактивный": # checking all statuses for this unit
                            if StaffStatus.objects.filter(staff_unit=units[i].id, status='active')\
                                or StaffStatus.objects.filter(staff_unit=units[i].id, status='temp')\
                                or StaffStatus.objects.filter(staff_unit=units[i].id, status='health'):
                                push_status = ('Сотрудник занят на другом проекте. Для изменения статуса сотрудник должен быть неактивен')
                                print(push_status)
                                continue
                        if units[i].status == "Активный":
                            try:
                                for work in Working.objects.filter(project_id=project.id):
                                    try:
                                        now = datetime.utcnow() + timedelta(hours=3)
                                        start_time = Graphik.objects.get(id=work.graphik_id).start_work
                                        wtime = start_time.strftime("%H:%M:%S")
                                        wdate = now.date()
                                        start_time = datetime.combine(wdate, start_time)
                                        now = datetime.utcnow() + timedelta(hours=3)
                                        amount = round(float((now - start_time).seconds) / 3600, 1) * 8/9
                                        end_time = now.strftime("%H:%M:%S")
                                        end_time = datetime.strptime(end_time, "%H:%M:%S")
                                        Graphik.objects.filter(id=work.graphik_id).update(stop_work=end_time, unit_hours_day_amount=amount)
                                        work.delete()
                                    except:
                                        print('The current work does not exist for this unit')
                            except:
                                print('The current work does not exist for this unit')
                        j = change_status(j)
                        StaffStatus.objects.filter(staff_unit=units[i].id, project=project).update(status=f"{j}")

                if len(request.POST.getlist('staff_added')) > 0:
                    for i in request.POST.getlist('staff_added'):
                        for unit in list_of_available:
                            if str(unit.id) == i:
                                #if StaffStatus.objects.filter(staff_unit=unit, project=project):
                                if unit in project.staff_list.filter():
                                    try:
                                        StaffStatus.objects.filter(staff_unit=unit, project=project).update(status="active")
                                    except:
                                        status = StaffStatus(staff_unit=unit, project=project, status="active")
                                        status.save()
                                else:
                                    project.staff_list.add(unit)
                                    status = StaffStatus(staff_unit=unit, project=project, status="active")
                                    status.save()


                name_project = request.POST['name_project']
                address_object = request.POST['address_object']
                curator_project = request.POST.getlist('curator_project')
                curator = StaffUnit.objects.get(unit_name=curator_project[0])
                gen_architector = request.POST.getlist('gen_architector')
                architector = StaffUnit.objects.get(unit_name=gen_architector[0])
                start_project = request.POST['start_project']
                end_project = request.POST['end_project']
                start_time = request.POST['start_time']
                end_time = request.POST['end_time']
                staff_list = request.POST.getlist('staff_list[]')

                p = Project.objects.filter(id=project_id).update(name_project=name_project, \
                        address_object=address_object, curator_project=curator, \
                        gen_architector=architector, start_project=start_project, \
                        end_project=end_project, start_time=start_time, end_time=end_time)


                return HttpResponseRedirect(reverse('projects_list'))

        return render(request, 'managing/project_change.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def project_add(request):
    if request.user.is_authenticated and request.user.is_superuser:
        list_of_available = []
        all_unit_list = []
        for unit in StaffUnit.objects.all():
            all_unit_list.append(unit)
            if not (StaffStatus.objects.filter(staff_unit=unit, status='active') \
                or StaffStatus.objects.filter(staff_unit=unit, status='temp') \
                or StaffStatus.objects.filter(staff_unit=unit, status='health')):
                list_of_available.append(unit)
        if request.POST:
            if 'save' in request.POST:
                name_project = request.POST['name_project']
                address_object = request.POST['address_object']
                try:
                    curator_project = request.POST.getlist('curator_project')
                    curator = StaffUnit.objects.get(unit_name=curator_project[0])
                except:
                    curator = ''
                try:
                    gen_architector = request.POST.getlist('gen_architector')
                    architector = StaffUnit.objects.get(unit_name=gen_architector[0])
                except:
                    architector = ''
                start_project = request.POST['start_project']
                end_project = request.POST['end_project']
                start_time = request.POST['start_time']
                end_time = request.POST['end_time']
                staff_list = request.POST.getlist('staff_added')
                add_staff_list = []
                for id in staff_list:
                    add_staff_list.append(StaffUnit.objects.get(id=id))

                errors = check_project_form(name_project, address_object, curator, \
                        architector, start_project, end_project, start_time, end_time)

                if len(errors) == 0:
                    project = Project(name_project=name_project, \
                            address_object=address_object, curator_project=curator, \
                            gen_architector=architector, start_project=start_project, \
                            end_project=end_project, start_time=start_time, end_time=end_time
                            )
                    project.save()
                    project.staff_list.set(add_staff_list)
                    # CHECK ALL STATUSES
                    if len(add_staff_list) > 0:
                        for unit in add_staff_list:
                            status = StaffStatus(staff_unit=unit, project=project, status="active")
                            status.save()
                    return HttpResponseRedirect(reverse('projects_list'))
                else:
                    message = f"Выявлено ошибок при заполнении: {len(errors)}"
        return render(request, 'managing/project_add.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def report(request, staff_id):
    if request.user.is_authenticated:
        staff = StaffUnit.objects.get(id=staff_id)
        graphiks = Graphik.objects.filter(unit_name=staff)
        projects = []
        for graphik in graphiks:
            if graphik.name_project not in projects:
                projects.append(graphik.name_project)
        report = []
        for project in projects:
            pgraphiks = Graphik.objects.filter(unit_name=staff, name_project=project)
            hours = 0
            amount = 0
            for graphik in pgraphiks:
                hours += graphik.unit_hours_day_amount
                amount += graphik.unit_hours_day_amount * graphik.unit_hour_salary
            date = f"{project.start_project.strftime('%d.%m.%Y')} - {project.end_project.strftime('%d.%m.%Y')}"
            report.append({'project': project.name_project, 'date': date, 'hours': hours, 'amount': amount})

        return render(request, 'managing/report.html', locals())


@csrf_protect
def projects_list(request):
    if request.user.is_authenticated and request.user.is_superuser:
        projects = Project.objects.all().exclude(flag=False)
        units = []
        for project in projects:
            start = project.start_project
            end = project.end_project
            project.amount = ((end - start).days + 1)
            units.append(project.staff_list.all())
        return render(request, 'managing/projects_list.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))


@csrf_protect
def staff_list(request):
    if request.user.is_authenticated and request.user.is_superuser:
        staff = StaffUnit.objects.all().exclude(flag=False)
        for unit in staff:
            try:
                status = StaffStatus.objects.filter(staff_unit=unit, status='active')
                unit.project = status[0].project.name_project
            except:
                unit.project = '-'
        return render(request, 'managing/staff_list.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))

@csrf_protect
def reports(request):
    date_first_mounth_day = get_first_mounth_date()
    date_end_mounth_day = get_end_mounth_date()
    projects_list = Project.objects.all()
    staff_list = StaffUnit.objects.all()
    if request.user.is_authenticated and request.user.is_superuser:
        if 'create' in request.POST:
            date_first_mounth_day = request.POST['date_start']
            date_end_mounth_day = request.POST['date_end']
            if not checking_interval_date(date_first_mounth_day, date_end_mounth_day):
                error = "Дата начала не может быть позже даты окончания"
                return render(request, 'managing/reports.html', locals())
            projects = request.POST.getlist('projects_list[]')
            staff = request.POST.getlist('staff_list[]')
            if len(projects) == 0:
                projects = [project.name_project for project in projects_list]
            if len(staff) == 0:
                staff = [staff.unit_name for staff in staff_list]
            report = []
            for project in projects:
                p = Project.objects.get(name_project=project)
                hallamount = 0
                sallamount = 0
                staff_in_project = []
                for unit in staff:
                    u = StaffUnit.objects.get(unit_name=unit)
                    g = Graphik.objects.filter(name_project=p, unit_name=u, work_date__range=[date_first_mounth_day, date_end_mounth_day])
                    if len(g) > 0:
                        hamount = 0
                        samount = 0
                        for i in g:
                            hamount += i.unit_hours_day_amount
                            samount += ((i.unit_hours_day_amount) * i.unit_hour_salary)
                            # salary_amount += unit_salary TODO new value Graphik in models
                        staff_in_project.append({'unit_name': unit, 'hours_amount': round(hamount, 1), 'salary_amount': samount})
                        hallamount += hamount
                        sallamount += samount
                if p.flag:
                    flag = 'Активный'
                else:
                    flag = 'Завершен'
                interval = f"{p.start_project.strftime('%d.%m.%y')} - {p.end_project.strftime('%d.%m.%y')}"
                report.append({'project_name': project, 'interval': interval, 'flag':flag, 'hours_amount': round(hallamount, 1), 'salary_amount': sallamount, 'units': staff_in_project})


        return render(request, 'managing/reports.html', locals())
    else:
        return HttpResponseRedirect(reverse('logining'))


@csrf_protect
def errors_page(request):
    return render(request, 'managing/errors_page.html', locals())
