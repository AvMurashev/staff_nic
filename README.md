# Staff_managing
Report card of staff

Technologies
---------
Python 3.7.3
Django
Heroku host
MySQL (nic.ru host)
Bootstrap
JS

To start
--------
1. cd staff/ pipenv shell
2. pip install -r requirements.txt

Result_site
----------------------------
https://staff-managing.herokuapp.com/
